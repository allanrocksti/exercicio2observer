package Principal;

import java.util.Observable;
import java.util.Observer;

public class Alarme implements Observer {

	private int num;
	public Alarme() {
		
	}
	public Alarme(int num) {
		this.setNum(num);
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void quandoSensorAtivar(Sensor sensor) {
		System.out.println("O Alarme"+(this.getNum() > 0?this.getNum():"")+" Tocou em resposta ao Sensor"+sensor.getNum());
	}
	public void desligarAlarme(Sensor sensor) {
		System.out.println("O Alarme"+(this.getNum() > 0?this.getNum():"")+" que foi ativado pelo sensor"+sensor.getNum()+ " foi desligado");
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		Sensor sensor = (Sensor) arg0;	
		if(String.valueOf(arg1) == "ligarAlarme") {
			quandoSensorAtivar(sensor);
		}else if(String.valueOf(arg1) == "desligarAlarme"){
			desligarAlarme(sensor);
		}
	}
	
}
