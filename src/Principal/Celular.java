package Principal;

import java.util.Observable;
import java.util.Observer;

public class Celular implements Observer{
	private int num;
	public Celular() {
		
	}
	public Celular(int num) {
		this.setNum(num);
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void quandoSensorAtivar(Sensor sensor) {
		System.out.println("O Celular"+(this.getNum() > 0?this.getNum():"")+" recebeu notificação: Alarme foi acionado. Sensor"+(sensor.getNum() > 0?sensor.getNum():"")+" Disparou o alarme"+(sensor.getNum() > 0?sensor.getNum():"")+". Ligando para a policia.");
	}
	public void verNotificacao(Sensor sensor) {
		System.out.println("O Celular"+(this.getNum() > 0?this.getNum():"")+" já vizualizou a notificação gerada pelo sistema após o sensor"+sensor.getNum()+" disparar o alarme. A ligação para a policia já foi terminada.");
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		Sensor sensor = (Sensor) arg0;	
		if(String.valueOf(arg1)=="ligarAlarme") {
			quandoSensorAtivar(sensor);
		}
		else if(String.valueOf(arg1)=="verNotificacao") {
			verNotificacao(sensor);
		}
	}
}
