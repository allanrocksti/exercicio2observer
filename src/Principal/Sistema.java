package Principal;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class Sistema {
	public static void main(String[] args) {
		
		Observer alarme1 = new Alarme(1);
		Observer celular1 = new Celular(1);
		Observer policia = new Policia();
		
		Observable sensor1 = new Sensor(1);
		
		sensor1.addObserver(policia);
		sensor1.addObserver(celular1);
		sensor1.addObserver(alarme1);
		
		Scanner scanner = new Scanner(System.in);
		
		String cmd = "";
		
		while (!cmd.equalsIgnoreCase("exit")) {
			System.out.print("Digite o comando: ");
			cmd = scanner.nextLine();
			
			String[] cmdSplited = cmd.split(" ");
			
			if (cmdSplited.length == 2) {				
				Sensor s = null;
				if (cmdSplited[1].equals("1"))
					s = (Sensor) sensor1;
				if (cmdSplited[0].equalsIgnoreCase("a")) {
					s.quandoaPazForpertubada();
				} else if (cmdSplited[0].equalsIgnoreCase("d")) {
					s.desligarAlarme();
				} else if (cmdSplited[0].equalsIgnoreCase("n")) {
					s.verNotificação();
				}else if(cmdSplited[0].equalsIgnoreCase("p")){
					s.policiaVindo();
				}
			}
				
			
		}
		System.out.println("Good by!");
		System.exit(0);
	}
}
