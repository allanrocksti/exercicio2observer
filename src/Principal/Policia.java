package Principal;

import java.util.Observable;
import java.util.Observer;

public class Policia implements Observer{
	private int num;
	public Policia() {
		
	}
	public Policia(int num) {
		this.setNum(num);
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void quandoSensorAtivar(Sensor sensor) {
		System.out.println("A chamada para a policia está sendo executada...");
		System.out.println("Policia atendeu");
	}
	public void policiaVindo(Sensor sensor) {
		System.out.println("Policia já se movendo para a casa...");
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		Sensor sensor = (Sensor) arg0;	
		if(String.valueOf(arg1)=="ligarAlarme") {
			quandoSensorAtivar(sensor);
		}
		else if(String.valueOf(arg1)=="policiaVindo") {
			policiaVindo(sensor);
		}
		
}
}
