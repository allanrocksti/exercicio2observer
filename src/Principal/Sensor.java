package Principal;

import java.util.Observable;

public class Sensor extends Observable{
	private int num;
	private String acao;
	
	public Sensor(int num) {
		this.setNum(num);
	}
	
	public void quandoaPazForpertubada() {
		System.out.println("Sensor"+this.getNum()+" foi ativado");
		acao = "ligarAlarme";
		notificar();
	}
	public void desligarAlarme() {
		acao = "desligarAlarme";
		notificar();
		
	}
	public void verNotificação() {
		acao = "verNotificacao";
		notificar();
	}
	public void policiaVindo() {
		acao = "policiaVindo";
		notificar();
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	public void notificar() {
		setChanged();
		notifyObservers(acao);
	}
}
